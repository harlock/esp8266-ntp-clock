/*
ESP8266 NTP Clock

Created 5 august 2018 by Pierre Brial

Based on Udp NTP Client :
	created 4 Sep 2010 by Michael Margolis
	modified 9 Apr 2012  by Tom Igoe
	updated for the ESP8266 12 Apr 2015 
	by Ivan Grokhotkov
*/

#include <ESP8266WiFi.h>
#include <WiFiUdp.h>
#include "LedControl.h"

#define TZ 4L	// Time Zone
#define INTERVAL 1800000L	// Contrôle NTP toutes les 30 mn

boolean error1=false;
unsigned long t=0,t0,tc0;

// LedControl(DIN,CLK,CS,numDevices);
LedControl lc=LedControl(D7,D6,D5,1);

char ssid[] = "MyWifiSSID";  	// your network SSID (name)
char pass[] = "MyWifiPassword"; // your network password

const char* ntpServerName = "pool.ntp.org";
const int NTP_PACKET_SIZE = 48; // NTP time stamp is in the first 48 bytes of the message

unsigned long getNTPtime()
	{
	byte packetBuffer[NTP_PACKET_SIZE]; //buffer to hold incoming and outgoing packets
	WiFiUDP udp;	// UDP instance to let us send and receive packets over UDP
	unsigned int localPort = 2390;      // local port to listen for UDP packets
	IPAddress timeServerIP;
	int cb=0;
	unsigned long
		highWord=0,
		lowWord=0,
		secsSince1900=0,
		secsSinceMidnight=0;
	
	WiFi.hostByName(ntpServerName, timeServerIP); 
		
	// send an NTP packet to a time server -------------------------------
	memset(packetBuffer, 0, NTP_PACKET_SIZE);
	// Initialize values needed to form NTP request
	packetBuffer[0] = 0b11100011;   // LI, Version, Mode
	packetBuffer[1] = 0;     // Stratum, or type of clock
	packetBuffer[2] = 6;     // Polling Interval
	packetBuffer[3] = 0xEC;  // Peer Clock Precision
	// 8 bytes of zero for Root Delay & Root Dispersion
	packetBuffer[12]  = 49;
	packetBuffer[13]  = 0x4E;
	packetBuffer[14]  = 49;
	packetBuffer[15]  = 52;
	// now you can send a packet requesting a timestamp:
	udp.begin(localPort);
	udp.beginPacket(timeServerIP, 123); //NTP requests are to port 123
	udp.write(packetBuffer, NTP_PACKET_SIZE);
	udp.endPacket();
	// -------------------------------------------------------------------
	
	// wait to see if a reply is available
	delay(500);
	cb = udp.parsePacket();
	if (cb==NTP_PACKET_SIZE)
		{
		udp.read(packetBuffer, NTP_PACKET_SIZE); // read the packet into the buffer
		//the timestamp starts at byte 40 of the received packet and the integer
		// part is four bytes, or two words, long. First, extract the two words:
		highWord = word(packetBuffer[40], packetBuffer[41]);
		lowWord = word(packetBuffer[42], packetBuffer[43]);
		// combine the four bytes (two words) into a long integer
		// and add 1 if fraction of seconds (byte 44) is greater than 0.5
		secsSince1900 = (highWord << 16 | lowWord);		// This is NTP time (seconds since Jan 1 1900)
		secsSinceMidnight = (secsSince1900+TZ*3600L) % 86400L * 100L + packetBuffer[44]*100L/256L; // centièmes de secondes depuis minuit
		}
	udp.flush();
	udp.stop();
	return secsSinceMidnight;
	}

void writeConnectOn7Segment(byte i)
	{
	lc.setRow(i,7,B1001110);	// C
	lc.setRow(i,6,0x1D);		// o
	lc.setRow(i,5,0x15);		// n
	lc.setRow(i,4,0x15);		// n
	lc.setChar(i,3,'E',false);	// E
	lc.setChar(i,2,'c',false);	// c
	lc.setRow(i,1,B10000111);	// t.
	lc.setChar(i,0,' ',true);	// .
	} 

void lcinit(byte i)
	{
	lc.shutdown(0,false);
	lc.setIntensity(0,i);
	lc.clearDisplay(0);
	}

void setup()
	{
	boolean b=0;
	pinMode(LED_BUILTIN, OUTPUT);	
	lcinit(0);
	writeConnectOn7Segment(0);
	WiFi.begin(ssid, pass);
	
	while (WiFi.status() != WL_CONNECTED)
		{
		delay(500);
		digitalWrite(LED_BUILTIN,b);
		b=1-b;
		lc.shutdown(0,b);
		}
	lc.shutdown(0,false);
	lc.clearDisplay(0);
	while(t==0) t=getNTPtime();
	t0=millis();
	tc0=t0;
	digitalWrite(LED_BUILTIN,HIGH);
	}

void loop()
	{
	byte h=0,m=0,s=0;
	unsigned int delta;
	unsigned long t1=millis(),ntp=0;
	
	if (t1<t0) t0=0;
	
	if ((t1-t0)>199L)
		{
		t=(t+(t1-t0)/10L)%8640000L; // t est en centièmes de secondes
		t0=t1;
		h=t / 360000L;
		m=(t % 360000L) / 6000L;
		s=(t % 6000L) / 100L;
		lc.setChar(0,7,h<10 ? 16 : h/10,false);
		lc.setDigit(0,6,h%10,false);
		lc.setDigit(0,4,m/10,false);
		lc.setDigit(0,3,m%10,false);
		lc.setDigit(0,1,s/10,false);
		lc.setDigit(0,0,s%10,error1);
		
		if ((t1 - tc0) > INTERVAL)	// Contrôle NTP toutes les 30 mn
			{
			tc0=t1;
			ntp=getNTPtime();

			if (ntp && t!=ntp)
				{
				error1=false;
				t=ntp;
				t0=millis();
				}
			else error1=true;
			
			lcinit((h>8)&&(h<18));
			}
		}
	}


	
