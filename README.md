# ESP8266 NTP Clock

An accurate ESP8266 clock based on Network Time Protocol.

## Circuit

![](../NTP-Clock.png)

### Components
* ESP8266 or ESP8285 board
* MAX7219 8 digits LED display module.

## Usage

* install [ledcontrol](https://github.com/harlock974/LedControl) library
* Add your Wifi network credentials in [NTPClient-LED-F.ino](../NTPClient-LED-F.ino) (lines 26-27)
* compile and upload program to the ESP8266
* power it.

